db.inventory.insertMany([
		{
			name: "Captain America Shield",
			price: 50000,
			qty : 17,
			company : "Hydra and Co",
		},
		{
			name: "Mjolnir",
			price: 75000,
			qty : 24,
			company : "Asgard Production",
		},
		{
			name: "Iron Man Suit",
			price:25400,
			qty : 25,
			company : "Stark Industries",
		},
		{
			name: "Eye of Agamoto",
			price: 28000,
			qty : 51,
			company : "Sanctum Company",
		},
		{
			name: "Iron Spider Suit",
			price: 30000,
			qty : 24,
			company : "Stark Industries",
		}
	])

// Query Operators 
	// allows us to be more flexible when querying in MongoDB, we can opt to find, update and delete documents based on some condition instead of just specific criterias.

// Comparison Query Operators
	// $gt and $gte
	// syntax:
		db.collections.find({field: {$gt: value}})
		db.collections.find({field: {$gte: value}})
		//ex
		db.inventory.find({price: {$gte: 75000}})

	//$lt and $lte - less than and less than or equal
	//syntax:
		db.collections.find({field :{$lt: value}})
		db.collections.find({field : {$lte: value}})
		//ex
		db.inventory.find({qty: {$lt: 20}})

	//$ne - not equal
	//syntax:
		db.inventory.find({qty: {$ne: 10}})

		//$in - allows us to find document that satisfy either of the two values
		//syntax:
		db.collections.find({field :{$in: value}})

		db.inventory.find({price: {$in: [25400, 30000]}})

			// *reminder: although, you can express this query using $or operator, choose the $in operator rather than $or opeartor when performing equality checks on the same field.


		db.inventory.find({price: {$gte: 50000}})

		db.inventory.find({qty: {$in: [24, 16]}})

//UPDATE AND DELETE

	db.inventory.updateMany({qty: {$lte: 24}}, {$set: {isActive: true}})

	db.inventory.updateMany({price: {$lt: 28000}}, {$set: {qty: 17}})

//LOGICAL OPERATORS
	// $and
	// syntax:
		db.collections.find({$and: [{criteria 1}, {criteria 2}]})

	//$and - allows us to return documents that satisfies all given conditions.
	
		db.inventory.find({$and: [{price: {$gte: 50000}}, {qty: 17}]})	

	//$or 
	//syntax:
		db.collections.find({$or: [{criteria1}, {criteria2}]})

		//$or - allows us to return documents that satisfies either of the given conditions.

		db.inventory.find({$or: [{qty: {$lt: 24}}, {isActive: false}]})

// mini activity
	
	db.inventory.find({$or: [{price: {$lte: 30000}}, {qty: {$lte: 24}, {$set: {isActive: true}}]})

	db.inventory.updateMany(
		{$or: 
			[
			{price: {$lte: 30000}}, 
			{qty: {$lte: 24}}, 
                    ]
        },
		{
			$set: {isActive: true}
		}
	)

// Evaluation Query Operator
	// $regex
		//syntax:
		{field: {$regex: /pattern/}}
			// case sensitive query
		{field: {$regex: /pattern/, $options: '$optionValue'}}
			// case insesitive query

			db.inventory.find(
				{
					name: {$regex: 'S'}
				}
			)


			db.inventory.find(
				{$and:
					[
						{
							name: {$regex: 'i', $options: '$i'}
						},
						{
							price: {$gt: 70000}
						}
					]
				}
			)

//FIELD PROJECTION
	// sllows us to hide.show properties of areturned documents after a query. When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.


	//Inclusion and Exclusion
		// Syntax:
			db.collections.find({crtiera}, {field: 1})
				// field: 1 - allows us to include/add specific fields only when retrieving documents. The value provided is 1 to denote that the field is being included.
			//ex
			db.inventory.find({}, {name: 1})

			db.inventory.find({}, {_id: 0, name:1, price:1})	

		// syntax:
			db.collections.find({criteria}, {field: 0})
				// field: 0 - allows us to exclude the specific field.	

			db.inventory.find({}, {qty: 0})	

			db.inventory.find({}, {_id: 0, name:0, price:0})	

			//reminder: when using filed projection, field inclusion and exclusion may not be used in the same time. excluding the "_id" field.


			db.inventory.find(
				{company: {regex: 'A'}}, 
				{name: 1, company:1, _id: 0}
				)

//Mini Activity

		db.inventory.find(
				{$and:
					[
						{name: {$regex: 'A'}},
						{price: {$lte: 30000}}
					]
				},
				{
					name: 1,
					price: 1,
					_id: 0
				}
			)
			

